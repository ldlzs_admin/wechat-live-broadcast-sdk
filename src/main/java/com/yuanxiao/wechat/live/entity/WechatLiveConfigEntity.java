package com.yuanxiao.wechat.live.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("WechatLiveConfigEntity")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WechatLiveConfigEntity {

    //微信号
    @ApiModelProperty("微信号")
    private String username;

    //昵称
    @ApiModelProperty("昵称")
    private String nickname;

    //用户列表
    @ApiModelProperty("用户列表")
    private List<User> users;

    //上架/下架 0下架 1上架
    @ApiModelProperty("上架/下架")
    private String onSale;

    //微信返回的商品ID
    @ApiModelProperty("微信返回的商品ID")
    private Long goodsId;

    //直播间ID
    @ApiModelProperty("直播间ID")
    private Integer id;

    //商品ID列表
    @ApiModelProperty("商品ID列表")
    private List<Long> ids;

    //直播间ID
    @ApiModelProperty("直播间ID")
    private Long roomId;

    //当action有值时该字段必填，直播间ID
    @ApiModelProperty("当action有值时该字段必填，直播间ID")
    private Long room_id;

    //只能填"get_replay"，表示获取回放
    @ApiModelProperty("回放/列表")
    private String action;

    //起始拉取视频，0表示从第一个视频片段开始拉取
    @ApiModelProperty("起始拉取视频，0表示从第一个视频片段开始拉取")
    private Integer start;

    //每次拉取的数量，建议100以内
    @ApiModelProperty("每次拉取的数量，建议100以内")
    private Integer limit;

	//直播间名称
    @ApiModelProperty("直播间名称")
    private String name;

	//背景图 存mediaId
    @ApiModelProperty("背景图 存mediaId")
    private String coverImg;

	//直播开始时间/分
    @ApiModelProperty("直播开始时间/分")
    private Integer startTime;

	//直播结束时间
    @ApiModelProperty("直播结束时间")
    private Integer endTime;

	//昵称
    @ApiModelProperty("昵称")
    private String anchorName;

	//主播微信号
    @ApiModelProperty("主播微信号")
    private String anchorWechat;

	//微信小号
    @ApiModelProperty("微信小号")
    private String subAnchorWechat;

	//创建者微信号
    @ApiModelProperty("创建者微信号")
    private String createrWechat;

	//分享图 存mediaId
    @ApiModelProperty("分享图 存mediaId")
    private String shareImg;

	//购物直播频道封面图 存mediaId
    @ApiModelProperty("购物直播频道封面图 存mediaId")
    private String feedsImg;

	//是否开启官方收录 【1: 开启，0：关闭】，默认开启收录
    @ApiModelProperty("是否开启官方收录 【1: 开启，0：关闭】，默认开启收录")
    private Integer isFeedsPublic;

	//直播间类型 【1: 推流，0：手机直播】 
    @ApiModelProperty("直播间类型 【1: 推流，0：手机直播】 ")
    private Integer type;

	//是否关闭点赞 【0：开启，1：关闭】（若关闭，观众端将隐藏点赞按钮，直播开始后不允许开启） 
    @ApiModelProperty("是否关闭点赞 【0：开启，1：关闭】（若关闭，观众端将隐藏点赞按钮，直播开始后不允许开启） ")
    private Integer closeLike;

	//是否关闭货架 【0：开启，1：关闭】（若关闭，观众端将隐藏商品货架，直播开始后不允许开启） 
    @ApiModelProperty("是否关闭货架 【0：开启，1：关闭】（若关闭，观众端将隐藏商品货架，直播开始后不允许开启） ")
    private Integer closeGoods;

	//是否关闭评论 【0：开启，1：关闭】（若关闭，观众端将隐藏评论入口，直播开始后不允许开启） 
    @ApiModelProperty("是否关闭评论 【0：开启，1：关闭】（若关闭，观众端将隐藏评论入口，直播开始后不允许开启） ")
    private Integer closeComment;

	//是否关闭回放 【0：开启，1：关闭】默认关闭回放（直播开始后允许开启） 
    @ApiModelProperty("是否关闭回放 【0：开启，1：关闭】默认关闭回放（直播开始后允许开启） ")
    private Integer closeReplay;

	//是否关闭分享 【0：开启，1：关闭】默认开启分享（直播开始后不允许修改） 
    @ApiModelProperty("是否关闭分享 【0：开启，1：关闭】默认开启分享（直播开始后不允许修改） ")
    private Integer closeShare;

	//是否关闭客服 【0：开启，1：关闭】 默认关闭客服（直播开始后允许开启） 
    @ApiModelProperty("是否关闭客服 【0：开启，1：关闭】 默认关闭客服（直播开始后允许开启） ")
    private Integer closeKf;
}
