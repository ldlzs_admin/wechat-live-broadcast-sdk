package com.yuanxiao.wechat.live.entity;

import lombok.Data;

@Data
public class GoodsInfo {
    private WechatGoodsEntity goodsInfo;
}
