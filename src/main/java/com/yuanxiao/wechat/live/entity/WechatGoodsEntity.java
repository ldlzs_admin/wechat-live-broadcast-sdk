package com.yuanxiao.wechat.live.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 商品
 *
 * @author wzw
 * @email sanaids@qq.com
 * @date 2023-11-21 22:45:45
 */
@ApiModel("WechatGoodsEntity")
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WechatGoodsEntity implements Serializable {

    //商品ID列表
    @ApiModelProperty("商品ID列表")
    private List<Long> goods_ids;

    //商品ID
    @ApiModelProperty("商品ID")
    private Long goodsId;

    //审核单ID
    @ApiModelProperty("审核单ID")
    private Long auditId;

	//mediaId存 商品图片
    @ApiModelProperty("mediaId存 商品图片")
    private String coverImgUrl;

	//商品名称
    @ApiModelProperty("商品名称")
    private String name;

	//价格类型，1：一口价（只需要传入price，price2不传） 2：价格区间（price字段为左边界，price2字段为右边界，price和price2必传） 3：显示折扣价（price字段为原价，price2字段为现价， price和price2必传）
    @ApiModelProperty("价格类型，1：一口价（只需要传入price，price2不传） 2：价格区间（price字段为左边界，price2字段为右边界，price和price2必传） 3：显示折扣价（price字段为原价，price2字段为现价， price和price2必传）")
    private Integer priceType;

	//数字，最多保留两位小数，单位元 
    @ApiModelProperty("数字，最多保留两位小数，单位元 ")
    private Double price;

	//数字，最多保留两位小数，单位元 
    @ApiModelProperty("数字，最多保留两位小数，单位元 ")
    private Double price2;

	//商品详情页的小程序路径，路径参数存在 url 的，该参数的值需要进行 encode 处理再填入 
    @ApiModelProperty("商品详情页的小程序路径，路径参数存在 url 的，该参数的值需要进行 encode 处理再填入 ")
    private String url;

	//当商品为第三方小程序的商品则填写为对应第三方小程序的appid，自身小程序商品则为'' 
    @ApiModelProperty("当商品为第三方小程序的商品则填写为对应第三方小程序的appid，自身小程序商品则为'' ")
    private String thirdPartyAppid;

}
