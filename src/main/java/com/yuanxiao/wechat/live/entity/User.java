package com.yuanxiao.wechat.live.entity;

import lombok.Data;

@Data
public class User {
    private String username;
    private String nickname;
}
