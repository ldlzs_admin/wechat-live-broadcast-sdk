package com.yuanxiao.wechat.live.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("WechatRoleConfigEntity")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WechatRoleConfigEntity {

    //用户名
    @ApiModelProperty("用户名")
    private String username;

    //角色
    @ApiModelProperty("角色")
    private Integer role;

    //微信号/昵称
    @ApiModelProperty("微信号/昵称")
    private String keyword;
}
