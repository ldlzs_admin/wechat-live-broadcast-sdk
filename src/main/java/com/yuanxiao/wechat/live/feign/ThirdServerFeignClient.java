package com.yuanxiao.wechat.live.feign;

import com.yuanxiao.common.core.result.R;
import com.yuanxiao.wechat.live.feign.fallback.ThirdServerFeignFallbackClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@FeignClient(value = "third-server", fallback = ThirdServerFeignFallbackClient.class)
@FeignClient(value = "lzsh-third", url = "http://localhost:53022/third", fallback = ThirdServerFeignFallbackClient.class)
@Qualifier("thirdServerFeignClient")
public interface ThirdServerFeignClient {

    @GetMapping("/wx/min/app/token")
    R getToken(
            @RequestParam(value = "clientId", required = false) String clientId
    );

}
