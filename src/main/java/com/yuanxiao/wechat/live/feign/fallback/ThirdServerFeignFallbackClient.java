package com.yuanxiao.wechat.live.feign.fallback;

import com.yuanxiao.common.core.result.R;
import com.yuanxiao.common.core.result.ResultCode;
import com.yuanxiao.wechat.live.feign.ThirdServerFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author: luozongsheng
 * @mail: lzs1115@foxmail.com
 * @date: 2022-05-31 16:40
 **/
@Component
@Slf4j
public class ThirdServerFeignFallbackClient implements ThirdServerFeignClient {

    @Override
    public R getToken(String clientId) {
        log.error("feign远程调用第三方服务异常后的降级方法");
        return R.failed(ResultCode.DEGRADATION);
    }
}
