package com.yuanxiao.wechat.live.util;

import lombok.Data;

import java.util.List;

@Data
public class WechatR {
    private String errCode;
    private String errMsg;
    private List<Object> roomInfo;
    private Integer total;
    private List<Object> liveReplay;
}
