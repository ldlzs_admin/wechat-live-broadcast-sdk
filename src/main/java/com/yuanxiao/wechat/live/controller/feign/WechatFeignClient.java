package com.yuanxiao.wechat.live.controller.feign;

import com.yuanxiao.common.core.result.R;
import com.yuanxiao.wechat.live.entity.GoodsInfo;
import com.yuanxiao.wechat.live.entity.WechatGoodsEntity;
import com.yuanxiao.wechat.live.entity.WechatLiveConfigEntity;
import com.yuanxiao.wechat.live.entity.WechatRoleConfigEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(value = "wechat", url = "https://api.weixin.qq.com")
@Qualifier("wechatFeignClient")
public interface WechatFeignClient {

    //获取临时素材
    @GetMapping("/cgi-bin/media/get?access_token={accessToken}&media_id={mediaId}")
    ResponseEntity<byte[]> getTempMedia(@PathVariable("mediaId") String mediaId, @PathVariable("accessToken") String accessToken);

    //获取token
    @GetMapping("/cgi-bin/token?grant_type={grantType}&appid={appId}&secret={secret}")
    R getToken(@PathVariable("grantType") String grantType, @PathVariable("appId") String appId, @PathVariable("secret") String secret);

    //----------------------------------------------直播间管理-----------------------------------------------------------

    //创建直播间
    @PostMapping("/wxaapi/broadcast/room/create?access_token={accessToken}")
    R createRoom(@RequestBody WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //编辑直播间
    @PostMapping("/wxaapi/broadcast/room/editroom?access_token={accessToken}")
    R editRoom(@RequestBody WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //删除直播间
    @PostMapping("/wxaapi/broadcast/room/deleteroom?access_token={accessToken}")
    R deleteRoom(WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //获取直播间列表和回放
    @PostMapping("/wxa/business/getliveinfo?access_token={accessToken}")
    R getLiveInfo(WechatLiveConfigEntity wechatLiveConfigEntity,@PathVariable("accessToken") String accessToken);

    //获取直播间推流地址
    @GetMapping("/wxaapi/broadcast/room/getpushurl?access_token={accessToken}")
    R getPushUrl(@RequestParam("roomId") Long roomId, @PathVariable("accessToken") String accessToken);

    //获取直播间分享二维码
    @GetMapping("/wxaapi/broadcast/room/getsharedcode?access_token={accessToken}")
    R getSharedCode(@RequestParam("roomId")Long roomId, @PathVariable("accessToken") String accessToken);

    //获取主播副号
    @GetMapping("/wxaapi/broadcast/room/getsubanchor?access_token={accessToken}")
    R getSubAnchor(@RequestParam("roomId") Long roomId, @PathVariable("accessToken") String accessToken);

    //修改主播副号(username = 微信号)
    @PostMapping("/wxaapi/broadcast/room/modifysubanchor?access_token={accessToken}")
    R modifySubAnchor(@RequestParam("roomId") Long roomId, @RequestParam("username") String username, @PathVariable("accessToken") String accessToken);

    //删除主播副号
    @PostMapping("/wxaapi/broadcast/room/deletesubanchor?access_token={accessToken}")
    R deleteSubAnchor(@RequestParam("roomId") Long roomId, @PathVariable("accessToken") String accessToken);

    //添加主播副号
    @PostMapping("/wxaapi/broadcast/room/addsubanchor?access_token={accessToken}")
    R addSubAnchor(@RequestParam("roomId") Long roomId, @RequestParam("username") String username, @PathVariable("accessToken") String accessToken);


    //添加管理直播间小助手(users的object对象需要属性 username=微信号   nickname=昵称)
    @PostMapping("/wxaapi/broadcast/room/addassistant?access_token={accessToken}")
    R addAssistant(WechatLiveConfigEntity wechatLiveConfigEntity,@PathVariable("accessToken") String accessToken);

    //修改直播间小助手
    @PostMapping("/wxaapi/broadcast/room/modifyassistant?access_token={accessToken}")
    R modifyAssistant(WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //删除直播间小助手
    @PostMapping("/wxaapi/broadcast/room/removeassistant?access_token={accessToken}")
    R removeAssistant(WechatLiveConfigEntity wechatLiveConfigEntity,@PathVariable("accessToken") String accessToken);

    //查询直播间小助手
    @GetMapping("/wxaapi/broadcast/room/getassistantlist?access_token={accessToken}")
    R getAssistantList(@RequestParam("roomId") Long roomId, @PathVariable("accessToken") String accessToken);

    //禁言管理(banComment 1-禁言，0-取消禁言)
    @PostMapping("/wxaapi/broadcast/room/updatecomment?access_token={accessToken}")
    R updateComment(@RequestParam("id") Long roomId, @RequestParam("banComment") Integer banComment, @PathVariable("accessToken") String accessToken);

    //官方收录管理
    @PostMapping("/wxaapi/broadcast/room/updatefeedpublic?access_token={accessToken}")
    R updateFeedPublic(@RequestParam("roomId") Long roomId, @RequestParam("isFeedsPublic") Integer isFeedsPublic, @PathVariable("accessToken") String accessToken);

    //客服功能管理
    @PostMapping("/wxaapi/broadcast/room/updatekf?access_token={accessToken}")
    R updateKF(@RequestParam("roomId") Long roomId, @RequestParam("closeKf") Integer closeKf, @PathVariable("accessToken") String accessToken);

    //回放功能管理
    @PostMapping("/wxaapi/broadcast/room/updatereplay?access_token={accessToken}")
    R updateReplay(@RequestParam("roomId") Long roomId, @RequestParam("closeReplay") Integer closeReplay, @PathVariable("accessToken") String accessToken);

    //导入商品
    @PostMapping("/wxaapi/broadcast/room/addgoods?access_token={accessToken}")
    R importGoods(WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //删除直播间商品
    @PostMapping("/wxaapi/broadcast/goods/deleteInRoom?access_token={accessToken}")
    R deleteGoods(WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //推送商品
    @PostMapping("/wxaapi/broadcast/goods/push?access_token={accessToken}")
    R pushGoods(WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //上下架商品
    @PostMapping("/wxaapi/broadcast/goods/onsale?access_token={accessToken}")
    R saleGoods(WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);

    //直播间商品排序
    @PostMapping("/wxaapi/broadcast/goods/sort?access_token={accessToken}")
    R sortGoods(@RequestParam("roomId") Long roomId, @RequestParam("goods") List<Long> goodsIds, @PathVariable("accessToken") String accessToken);

    //下载商品讲解视频
    @PostMapping("/wxaapi/broadcast/goods/getVideo?access_token={accessToken}")
    R downloadGoodsVideo(WechatLiveConfigEntity wechatLiveConfigEntity, @PathVariable("accessToken") String accessToken);


    //----------------------------------------------商品管理-----------------------------------------------------------

    //添加并提审商品
    @PostMapping("/wxaapi/broadcast/goods/add?access_token={accessToken}")
    R addGoods(@RequestBody GoodsInfo goodsInfo, @PathVariable("accessToken") String accessToken);

    //重新提交商品审核
    @PostMapping("/wxaapi/broadcast/goods/audit?access_token={accessToken}")
    R resubmitGoodsAudit(WechatGoodsEntity wechatGoodsEntity, @PathVariable("accessToken") String accessToken);

    //获取商品的信息与审核状态
    @PostMapping("/wxa/business/getgoodswarehouse?access_token={accessToken}")
    R getGoodsAuditInfo(WechatGoodsEntity wechatGoodsEntity, @PathVariable("accessToken") String accessToken);

    //撤回商品审核
    @PostMapping("/wxaapi/broadcast/goods/resetaudit?access_token={accessToken}")
    R resetGoodsAudit(WechatGoodsEntity wechatGoodsEntity, @PathVariable("accessToken") String accessToken);

    //更新商品
    @PostMapping("/wxaapi/broadcast/goods/update?access_token={accessToken}")
    R updateGoodsinfo(@RequestBody GoodsInfo goodsInfo, @PathVariable("accessToken") String accessToken);


    /**
     * @param offset      分页条数起点
     * @param limit       分页大小，默认30，不超过100
     * @param status      商品状态，0：未审核。1：审核中，2：审核通过，3：审核驳回
     * @param accessToken token
     * @return X
     */
    //获取商品列表
    @GetMapping("/wxaapi/broadcast/goods/getapproved?access_token={accessToken}")
    R getGoodsInfo(@RequestParam("offset") Integer offset, @RequestParam("limit") Integer limit, @RequestParam("status") Integer status, @PathVariable("accessToken") String accessToken);


    //删除商品
    @PostMapping("/wxaapi/broadcast/goods/delete?access_token={accessToken}")
    R deleteGoodsInfo(WechatGoodsEntity wechatGoodsEntity, @PathVariable("accessToken") String accessToken);


    //----------------------------------------------成员管理-----------------------------------------------------------

    //设置成员角色(role 取值[1-管理员，2-主播，3-运营者]，设置超级管理员将无效)
    @PostMapping("/wxaapi/broadcast/role/addrole?access_token={accessToken}")
    R addRole(WechatRoleConfigEntity wechatRoleConfigEntity, @PathVariable("accessToken") String accessToken);


    //移除成员角色
    @PostMapping("/wxaapi/broadcast/role/deleterole?access_token={accessToken}")
    R deleteRole(WechatRoleConfigEntity wechatRoleConfigEntity, @PathVariable("accessToken") String accessToken);

    /**
     *
     * @param accessToken token
     * @return X
     */
    //查询成员列表
    @GetMapping("/wxaapi/broadcast/role/getrolelist?access_token={accessToken}")
    R getRoleList(@RequestParam("role")Integer role,@RequestParam("keyword")String keyword,
                  @PathVariable("accessToken") String accessToken);


    //----------------------------------------------长期订阅-----------------------------------------------------------

    //发送直播开始事件(userOpenId = 接收该群发开播事件的订阅用户OpenId列表)
    @PostMapping("/wxa/business/push_message?access_token={accessToken}")
    R pushMessage(@RequestParam("room_id")Long roomId, @RequestParam("user_openid") List<String> userOpenIds, @PathVariable("accessToken") String accessToken);

    /**
     *
     * @param pageBreak 翻页标记，获取第一页时不带，第二页开始需带上上一页返回结果中的page_break
     * @param limit 获取长期订阅用户的个数限制，默认200，最大2000
     * @param accessToken token
     * @return really?
     */
    //获取长期订阅用户
    @PostMapping("/wxa/business/push_message?access_token={accessToken}")
    R getFollowers(@RequestParam(name = "page_break",required = false)Integer pageBreak,@RequestParam(name = "limit",required = false)Integer limit, @PathVariable("accessToken") String accessToken);
}
