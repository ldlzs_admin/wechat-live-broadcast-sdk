package com.yuanxiao.wechat.live.controller.feign;

import cn.hutool.json.JSONObject;
import com.yuanxiao.common.core.result.R;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "goods", url = "http://localhost:53030")
@Qualifier("goodsFeignClient")
public interface GoodsFeignClient {
//    @GetMapping("/server/call/goods/shop/sku/shop/spu/page")
//    R getGoodsPage(
//            @RequestParam("bizDeptId")Long deptId,
//            @RequestParam("page")String page,
//            @RequestParam("limit")String limit,
//            @RequestParam("searchKey")String searchKey);
    @GetMapping("/server/call/goods/shop/sku/shop/spu/page")
    R getGoodsPage(@RequestBody JSONObject jsonObject);
}
